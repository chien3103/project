<?php

use App\Http\Controllers\schemaDemo;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// //tao router
//  Route::get("/vietPro",function(){
//      echo "Hello world";
//  });
// //truyền biến cho router
//  Route::get("/xinChao/{name}",function($name){
//     echo "Xin chào: ".$name;
//  });

// //truyền nhiều tham số cho router
// Route::get('hello/{name}/{year}',function($name,$year){
//     echo "Hello: ".$name."<br>"."Year: ".$year;
// });
// //Truyền tham số mặc định
// Route::get('hello2/{name?}/{year?}',function($name="Tưởng Văn Huế",$year="2030"){
//     echo "Hello: ".$name."<br>"."Year: ".$year;
// });
// //dinh danh cho router

// Route::get('trangChu',function(){
//     echo "Đây là trang chủ";
// });
// //Chuyển hướng
// Route::get('trangNguon',function(){
//     return redirect("trangChu");
// });

//tao router
//     Route::get('hello ', function () {
//         echo "Hello word Chiến";
//     });
// //Truyền biến cho Router
//     Route::get('tinhToan/{a}/{b}/{phepTinh?}',function($a,$b,$pt="x"){

//         // if($pt = "+"){
//         //     echo $a + $b ;
//         //     echo "<br>";
//         // }
//         // elseif($pt="-"){
//         //     echo $a - $b;
//         //     echo "<br>";
//         // }
//         // elseif($pt = "x"){
//         //     echo $a*$b;
//         //     echo "<br>";
//         // }
//         // elseif($pt=":"){
//         //     echo $a/$b;
//         // }
//             switch ($pt) {
//                 case '+':
//                     echo $a + $b;
//                     break;
//                 case '-':
//                     echo $a - $b;
//                     break;
//                 case 'x':
//                     echo $a * $b;
//                     break;
//                 case ':':
//                     echo $a / $b;
//                     break;
//                 default:
//                     echo $a +$b;
//                     break;
//             }

//     });
    // //dinh danh cho route
    //     Route::get('trangDich', function () {
    //         echo "Đây là trang đích";
    //     })->name('dich');
    //     Route::get('trangNguon', function () {
    //         echo "đây là trang nguồn";
    //        //cách 1 return redirect()->route('dich') ;
    //        //cách 2
    //        return redirect('trangDich');
    //     });
    // //Route group
    //     Route::group(['prefix' => 'product'], function () {
    //         Route::get('list',function(){echo"đây là trang list";})->name('list');
    //         Route::get('edit',function(){
    //             //echo"đây là trang edit";
    //            return redirect()->route('list');
    //         });
    //     });


    //Route ket hop cung controller
       //Route::get("trangChu","demoController@trangChu");
     //truyền biến cho controller
        //Route::get('demKyTu/{chuoi?}',"demoController@demKyTu");
    //view laravel
       //Route::get('trangview', "viewContrller@viewDemo");
    //truyền biền cho view
        //Route::get('truyenBien/{a}/{b}',"viewContrller@phuongTrinhBacNhat");

        //VALIDATION
    // Route::get('login','loginController@login');
    // Route::post('login','loginController@processLogin');
    //BAI TAP TU LAM GIAI PHUONG TRINH BAC 2
        // Route::get('phuongTrinhBac2','phuongTrinhController@phuongTrinhBac2');
        // Route::post('phuongTrinhBac2','phuongTrinhController@process');




    //SCHEMA TABLE

        Route::group(['prefix' => 'schema'], function () {
            Route::get('crateTable', 'schemaDemo@crateTable' );
            Route::get('renameTable', 'schemaDemo@renameTable' );
            Route::get('sanPham','schemaDemo@sanPham');
            Route::get('dropTable','schemaDemo@dropTable');
            //foreign key
            Route::get('bang1','schemaDemo@bang1');
            Route::get('bang2','schemaDemo@bang2');
            Route::get('foreignKey','schemaDemo@foreignKey');
        });
        Route::group(['prefix' => 'query'], function () {
            Route::get('insert','queryDemo@insert');
            Route::post('insert','queryDemo@processinsert');
            Route::get('update/{id}','queryDemo@update');
        });


