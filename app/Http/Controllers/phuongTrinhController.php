<?php

namespace App\Http\Controllers;
use App\Http\Requests\phuongTrinh2;
use Illuminate\Http\Request;

class phuongTrinhController extends Controller
{
    public function phuongTrinhBac2()
    {
        return view('phuongTrinh.phuongTrinh2') ;
    }
    public function process(phuongTrinh2 $request)
    {
      $a = $request->a;
      $b = $request->b;
      $c = $request->c;
      $deta = ($b*$b) - (4*$a*$c);
      //$result = "";
      if( $deta < 0){
        $result = "Phương trình vô nghiệm";
      }
      if( $deta == 0){
        $x = -($b)/(2*$a);
        $result = $x ;
      }
     if($deta > 0){
        $x1 = -($b)/(2*$a);
        $x2 = -(($b*$b)-sqrt($deta))/(2*$a);
        //$result= "sadasdsa";
        $result = "x1=".$x1."------"."x2=".$x2 ;
      }

        return view('phuongTrinh.phuongTrinh2',['nghiem'=>$result]);
    }
}
