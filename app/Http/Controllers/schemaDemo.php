<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class schemaDemo extends Controller
{
    //tao bang
    public function crateTable()
    {
        Schema::create('khachHang', function (Blueprint $table) {
            $table->increments('maNhanVien');
            $table->string('email')->nullable()->default('tuongchien31031999@gmail.com')->unique();
            $table->string('matKhau')->nullable();
            $table->integer('tuoi')->unsigned();
            $table->date('ngaySinh');
            $table->timestamps();

        });

    }
    //sửa bảng
    public function renameTable()
    {
        Schema::rename('sanPham', 'san_pham');
    }
    //tạo primary key không cần increments
    public function sanPham()
    {
        Schema::create('sanPham', function (Blueprint $table) {
            $table->integer('maSanPham')->unsigned();
            $table->string('tenSanPham')->nullable();
            $table->float('giá')->default(10000);
            $table->text('moTa')->default('hàng còn tốt lắm');
            //1 khóa chính
            $table->primary('maSanPham');
            //2 khóa chính
            // $table->primary(['cột_1','cột_2']);
        });
    }

    public function dropTable()
    {
        Schema::dropIfExists('san_pham');
    }
    public function bang1()
    {
        Schema::create('bang1', function (Blueprint $table) {
            $table->increments('bang1_id');
            $table->string('email')->nullable()->default('tuongchien31031999@gmail.com');
            $table->string('password', 100)->nullable();
            $table->tinyInteger('level');
        });
    }
    public function bang2()
    {
       Schema::create('bang2', function (Blueprint $table) {
           $table->increments('bang2_id');
           $table->integer('soDienThoai')->default("0986752674");
           $table->text('diaChi')->default('Hà nội');
           $table->integer('bang1_id')->unsigned();

       });
    }
    public function foreignKey()
    {
        Schema::table('bang2', function (Blueprint $table) {
            $table->foreign('bang1_id')->references('bang1_id')->on('bang1')->onDelete('cascade');
        });
    }

}
