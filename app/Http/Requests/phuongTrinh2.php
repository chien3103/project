<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class phuongTrinh2 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'a'=>"required|numeric",
            'b'=>"required|numeric",
            'c'=>"required|numeric"
        ];
    }
    public function messages()
    {
        return [
            'a.required' => "nhập số a",
            'a.numeric' => " a phải là số",
            'b.required' => "nhập số b",
            'b.numeric' => " b phải là số",
            'c.required' => "nhập số c",
            'c.numeric' => "c phải là số",
        ];


    }

}
