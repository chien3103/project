<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class loginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|email|min:3',
            'passwrod' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Tài khoản không được trống',
            'username.email'    => 'Tài khoản phải là email',
            'username.min'      => 'Tài khoản tối thiểu là 3 ký tự',
            'passwrod.required' => 'mật khẩu không được trống'
        ];
    }
}
