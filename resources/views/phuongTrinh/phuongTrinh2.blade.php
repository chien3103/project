<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>Phuong trinh bac 2 co dang ax.x + bx +c = 0 </h3>
    <form action="" method="post">
        @csrf
       So a: <input type="text" name="a" id=""><br>

       @if ($errors->has('a'))
        {{ $errors->first('a') }}
        <br>
       @endif
       So b: <input type="text" name="b" id=""><br>

        @if ($errors->has('b'))
        {{ $errors->first('b') }}
        <br>
        @endif

       So c: <input type="text" name="c" id=""><br>

       @if ($errors->has('c'))
       {{ $errors->first('c') }}
       <br>
       @endif
       <button >Tinh</button>
    </form>
        @if (isset($nghiem))
        <p>Phương trình có nghiệm là:{{ $nghiem }}</p>
        @endif

</body>
</html>
